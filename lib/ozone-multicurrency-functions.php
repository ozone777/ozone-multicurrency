<?php

///////////////////////////////
//DEBUG FUNCTION LOGIC
///////////////////////////////


if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

function my_plugin_activate() {

  	//SET DEFAULT CURRENCY
	$odefault_currency = get_option('odefault_currency');
	if(!isset($odefault_currency))
  		add_option('odefault_currency','USD');
	  
}

register_activation_hook( __FILE__, 'my_plugin_activate' );

if ( ! function_exists('get_current_url')) {
	function get_current_url(){
		$url = $_SERVER["REQUEST_URI"];
		$url = strtok($url, '?');
		$last_char = substr($url, -1);
		if($last_char=="/")
			$url = substr_replace($url ,"", -1);
		if($url =="")
			return "/home";
		return $url;
	}
}

function ozone_get_woocommerce_currencies() {
	static $currencies;

	if ( ! isset( $currencies ) ) {
		$currencies = array_unique(
			apply_filters(
				'woocommerce_currencies',
				array(
					'AED' => 'United Arab Emirates dirham',
					'AFN' => 'Afghan afghani',
					'ALL' => 'Albanian lek',
					'AMD' => 'Armenian dram',
					'ANG' => 'Netherlands Antillean guilder',
					'AOA' => 'Angolan kwanza',
					'ARS' => 'Argentine peso',
					'AUD' => 'Australian dollar',
					'AWG' => 'Aruban florin',
					'AZN' => 'Azerbaijani manat',
					'BAM' => 'Bosnia and Herzegovina convertible mark',
					'BBD' => 'Barbadian dollar',
					'BDT' => 'Bangladeshi taka',
					'BGN' => 'Bulgarian lev',
					'BHD' => 'Bahraini dinar',
					'BIF' => 'Burundian franc',
					'BMD' => 'Bermudian dollar',
					'BND' => 'Brunei dollar',
					'BOB' => 'Bolivian boliviano',
					'BRL' => 'Brazilian real',
					'BSD' => 'Bahamian dollar',
					'BTC' => 'Bitcoin',
					'BTN' => 'Bhutanese ngultrum',
					'BWP' => 'Botswana pula',
					'BYR' => 'Belarusian ruble (old)',
					'BYN' => 'Belarusian ruble',
					'BZD' => 'Belize dollar',
					'CAD' => 'Canadian dollar',
					'CDF' => 'Congolese franc',
					'CHF' => 'Swiss franc', 
					'CLP' => 'Chilean peso', 
					'CNY' => 'Chinese yuan', 
					'COP' => 'Peso Colombiano', 
					'CRC' => 'Costa Rican col&oacute;n', 
					'CUC' => 'Cuban convertible peso', 
					'CUP' => 'Cuban peso', 
					'CVE' => 'Cape Verdean escudo', 
					'CZK' => 'Czech koruna', 
					'DJF' => 'Djiboutian franc', 
					'DKK' => 'Danish krone', 
					'DOP' => 'Dominican peso',
					'DZD' => 'Algerian dinar', 
					'EGP' => 'Egyptian pound', 
					'ERN' => 'Eritrean nakfa',
					'ETB' => 'Ethiopian birr', 
					'EUR' => 'Euro', 
					'FJD' => 'Fijian dollar',
					'FKP' => 'Falkland Islands pound',
					'GBP' => 'Pound sterling',
					'GEL' => 'Georgian lari', 
					'GGP' => 'Guernsey pound', 
					'GHS' => 'Ghana cedi', 
					'GIP' => 'Gibraltar pound',
					'GMD' => 'Gambian dalasi', 
					'GNF' => 'Guinean franc', 
					'GTQ' => 'Guatemalan quetzal',
					'GYD' => 'Guyanese dollar', 
					'HKD' => 'Hong Kong dollar', 
					'HNL' => 'Honduran lempira', 
					'HRK' => 'Croatian kuna',
					'HTG' => 'Haitian gourde', 
					'HUF' => 'Hungarian forint', 
					'IDR' => 'Indonesian rupiah', 
					'ILS' => 'Israeli new shekel', 
					'IMP' => 'Manx pound', 
					'INR' => 'Indian rupee', 
					'IQD' => 'Iraqi dinar', 
					'IRR' => 'Iranian rial', 
					'IRT' => 'Iranian toman',
					'ISK' => 'Icelandic kr&oacute;na', 
					'JEP' => 'Jersey pound', 
					'JMD' => 'Jamaican dollar',
					'JOD' => 'Jordanian dinar', 
					'JPY' => 'Japanese yen', 
					'KES' => 'Kenyan shilling', 
					'KGS' => 'Kyrgyzstani som', 
					'KHR' => 'Cambodian riel',
					'KMF' => 'Comorian franc', 
					'KPW' => 'North Korean won', 
					'KRW' => 'South Korean won', 
					'KWD' => 'Kuwaiti dinar',
					'KYD' => 'Cayman Islands dollar',
					'KZT' => 'Kazakhstani tenge',
					'LAK' => 'Lao kip', 
					'LBP' => 'Lebanese pound', 
					'LKR' => 'Sri Lankan rupee',
					'LRD' => 'Liberian dollar',
					'LSL' => 'Lesotho loti',
					'LYD' => 'Libyan dinar', 
					'MAD' => 'Moroccan dirham', 
					'MDL' => 'Moldovan leu', 
					'MGA' => 'Malagasy ariary', 
					'MKD' => 'Macedonian denar',
					'MMK' => 'Burmese kyat',
					'MNT' => 'Mongolian t&ouml;gr&ouml;g',
					'MOP' => 'Macanese pataca', 
					'MRU' => 'Mauritanian ouguiya',
					'MUR' => 'Mauritian rupee',
					'MVR' => 'Maldivian rufiyaa', 
					'MWK' => 'Malawian kwacha',
					'MXN' => 'Mexican peso',
					'MYR' => 'Malaysian ringgit',
					'MZN' => 'Mozambican metical',
					'NAD' => 'Namibian dollar',
					'NGN' => 'Nigerian naira', 
					'NIO' => 'Nicaraguan c&oacute;rdoba',
					'NOK' => 'Norwegian krone',
					'NPR' => 'Nepalese rupee', 
					'NZD' => 'New Zealand dollar',
					'OMR' => 'Omani rial',
					'PAB' => 'Panamanian balboa', 
					'PEN' => 'Sol',
					'PGK' => 'Papua New Guinean kina', 
					'PHP' => 'Philippine peso', 
					'PKR' => 'Pakistani rupee',
					'PLN' => 'Polish z&#x142;oty', 
					'PRB' => 'Transnistrian ruble', 
					'PYG' => 'Paraguayan guaran&iacute;', 
					'QAR' => 'Qatari riyal',
					'RON' => 'Romanian leu',
					'RSD' => 'Serbian dinar', 
					'RUB' => 'Russian ruble', 
					'RWF' => 'Rwandan franc',
					'SAR' => 'Saudi riyal', 
					'SBD' => 'Solomon Islands dollar', 
					'SCR' => 'Seychellois rupee',
					'SDG' => 'Sudanese pound', 
					'SEK' => 'Swedish krona', 
					'SGD' => 'Singapore dollar',
					'SHP' => 'Saint Helena pound', 
					'SLL' => 'Sierra Leonean leone', 
					'SOS' => 'Somali shilling', 
					'SRD' => 'Surinamese dollar',
					'SSP' => 'South Sudanese pound',
					'STN' => 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe dobra', 
					'SYP' => 'Syrian pound',
					'SZL' => 'Swazi lilangeni', 
					'THB' => 'Thai baht', 
					'TJS' => 'Tajikistani somoni', 
					'TMT' => 'Turkmenistan manat', 
					'TND' => 'Tunisian dinar', 
					'TOP' => 'Tongan pa&#x2bb;anga', 
					'TRY' => 'Turkish lira', 
					'TTD' => 'Trinidad and Tobago dollar', 
					'TWD' => 'New Taiwan dollar',
					'TZS' => 'Tanzanian shilling', 
					'UAH' => 'Ukrainian hryvnia',
					'UGX' => 'Ugandan shilling', 
					'USD' => 'United States (US) dollar', 
					'UYU' => 'Uruguayan peso', 
					'UZS' => 'Uzbekistani som', 
					'VEF' => 'Venezuelan bol&iacute;var', 
					'VES' => 'Bol&iacute;var soberano', 
					'VND' => 'Vietnamese &#x111;&#x1ed3;ng', 
					'VUV' => 'Vanuatu vatu', 
					'WST' => 'Samoan t&#x101;l&#x101;',
					'XAF' => 'Central African CFA franc', 
					'XCD' => 'East Caribbean dollar',
					'XOF' => 'West African CFA franc', 
					'XPF' => 'CFP franc', 
					'YER' => 'Yemeni rial', 
					'ZAR' => 'South African rand', 
					'ZMW' => 'Zambian kwacha', 
				)
			)
		);
	}

	return $currencies;
}



function ozone_get_woocommerce_currency_symbols() {

	$symbols = apply_filters(
		'woocommerce_currency_symbols',
		array(
			'AED' => '&#x62f;.&#x625;',
			'AFN' => '&#x60b;',
			'ALL' => 'L',
			'AMD' => 'AMD',
			'ANG' => '&fnof;',
			'AOA' => 'Kz',
			'ARS' => '&#36;',
			'AUD' => '&#36;',
			'AWG' => 'Afl.',
			'AZN' => 'AZN',
			'BAM' => 'KM',
			'BBD' => '&#36;',
			'BDT' => '&#2547;&nbsp;',
			'BGN' => '&#1083;&#1074;.',
			'BHD' => '.&#x62f;.&#x628;',
			'BIF' => 'Fr',
			'BMD' => '&#36;',
			'BND' => '&#36;',
			'BOB' => 'Bs.',
			'BRL' => '&#82;&#36;',
			'BSD' => '&#36;',
			'BTC' => '&#3647;',
			'BTN' => 'Nu.',
			'BWP' => 'P',
			'BYR' => 'Br',
			'BYN' => 'Br',
			'BZD' => '&#36;',
			'CAD' => '&#36;',
			'CDF' => 'Fr',
			'CHF' => '&#67;&#72;&#70;',
			'CLP' => '&#36;',
			'CNY' => '&yen;',
			'COP' => '&#36;',
			'CRC' => '&#x20a1;',
			'CUC' => '&#36;',
			'CUP' => '&#36;',
			'CVE' => '&#36;',
			'CZK' => '&#75;&#269;',
			'DJF' => 'Fr',
			'DKK' => 'DKK',
			'DOP' => 'RD&#36;',
			'DZD' => '&#x62f;.&#x62c;',
			'EGP' => 'EGP',
			'ERN' => 'Nfk',
			'ETB' => 'Br',
			'EUR' => '&euro;',
			'FJD' => '&#36;',
			'FKP' => '&pound;',
			'GBP' => '&pound;',
			'GEL' => '&#x20be;',
			'GGP' => '&pound;',
			'GHS' => '&#x20b5;',
			'GIP' => '&pound;',
			'GMD' => 'D',
			'GNF' => 'Fr',
			'GTQ' => 'Q',
			'GYD' => '&#36;',
			'HKD' => '&#36;',
			'HNL' => 'L',
			'HRK' => 'kn',
			'HTG' => 'G',
			'HUF' => '&#70;&#116;',
			'IDR' => 'Rp',
			'ILS' => '&#8362;',
			'IMP' => '&pound;',
			'INR' => '&#8377;',
			'IQD' => '&#x639;.&#x62f;',
			'IRR' => '&#xfdfc;',
			'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
			'ISK' => 'kr.',
			'JEP' => '&pound;',
			'JMD' => '&#36;',
			'JOD' => '&#x62f;.&#x627;',
			'JPY' => '&yen;',
			'KES' => 'KSh',
			'KGS' => '&#x441;&#x43e;&#x43c;',
			'KHR' => '&#x17db;',
			'KMF' => 'Fr',
			'KPW' => '&#x20a9;',
			'KRW' => '&#8361;',
			'KWD' => '&#x62f;.&#x643;',
			'KYD' => '&#36;',
			'KZT' => '&#8376;',
			'LAK' => '&#8365;',
			'LBP' => '&#x644;.&#x644;',
			'LKR' => '&#xdbb;&#xdd4;',
			'LRD' => '&#36;',
			'LSL' => 'L',
			'LYD' => '&#x644;.&#x62f;',
			'MAD' => '&#x62f;.&#x645;.',
			'MDL' => 'MDL',
			'MGA' => 'Ar',
			'MKD' => '&#x434;&#x435;&#x43d;',
			'MMK' => 'Ks',
			'MNT' => '&#x20ae;',
			'MOP' => 'P',
			'MRU' => 'UM',
			'MUR' => '&#x20a8;',
			'MVR' => '.&#x783;',
			'MWK' => 'MK',
			'MXN' => '&#36;',
			'MYR' => '&#82;&#77;',
			'MZN' => 'MT',
			'NAD' => 'N&#36;',
			'NGN' => '&#8358;',
			'NIO' => 'C&#36;',
			'NOK' => '&#107;&#114;',
			'NPR' => '&#8360;',
			'NZD' => '&#36;',
			'OMR' => '&#x631;.&#x639;.',
			'PAB' => 'B/.',
			'PEN' => 'S/',
			'PGK' => 'K',
			'PHP' => '&#8369;',
			'PKR' => '&#8360;',
			'PLN' => '&#122;&#322;',
			'PRB' => '&#x440;.',
			'PYG' => '&#8370;',
			'QAR' => '&#x631;.&#x642;',
			'RMB' => '&yen;',
			'RON' => 'lei',
			'RSD' => '&#1088;&#1089;&#1076;',
			'RUB' => '&#8381;',
			'RWF' => 'Fr',
			'SAR' => '&#x631;.&#x633;',
			'SBD' => '&#36;',
			'SCR' => '&#x20a8;',
			'SDG' => '&#x62c;.&#x633;.',
			'SEK' => '&#107;&#114;',
			'SGD' => '&#36;',
			'SHP' => '&pound;',
			'SLL' => 'Le',
			'SOS' => 'Sh',
			'SRD' => '&#36;',
			'SSP' => '&pound;',
			'STN' => 'Db',
			'SYP' => '&#x644;.&#x633;',
			'SZL' => 'L',
			'THB' => '&#3647;',
			'TJS' => '&#x405;&#x41c;',
			'TMT' => 'm',
			'TND' => '&#x62f;.&#x62a;',
			'TOP' => 'T&#36;',
			'TRY' => '&#8378;',
			'TTD' => '&#36;',
			'TWD' => '&#78;&#84;&#36;',
			'TZS' => 'Sh',
			'UAH' => '&#8372;',
			'UGX' => 'UGX',
			'USD' => '&#36;',
			'UYU' => '&#36;',
			'UZS' => 'UZS',
			'VEF' => 'Bs F',
			'VES' => 'Bs.S',
			'VND' => '&#8363;',
			'VUV' => 'Vt',
			'WST' => 'T',
			'XAF' => 'CFA',
			'XCD' => '&#36;',
			'XOF' => 'CFA',
			'XPF' => 'Fr',
			'YER' => '&#xfdfc;',
			'ZAR' => '&#82;',
			'ZMW' => 'ZK',
		)
	);

	return $symbols;
}	


///////////////////////////////
//SELECT CURRENCY LOGIC
///////////////////////////////


//SET CSS SELECT
function ozone_web_evo_scripts() {
    wp_enqueue_style('oselect_css', plugins_url('/ozone-multicurrency/css/oselect_css.css'));
}
add_action('wp_enqueue_scripts', 'ozone_web_evo_scripts');

//GET OCURRENCY
//ESTE METODO OBTIENE DE MANERA OPTIMA LA MONEDA SELECCIONADA





////SET CURRENCY COOKIE
function set_ocurrency_param() 
{
	if(isset($_GET["ocurrency"])){
		unset($_COOKIE['ocurrency']);
		$site_url = get_site_url();
		$site_url = str_replace("http://","",$site_url);
		$site_url = str_replace("https://","",$site_url);
	
		setcookie ( 'ocurrency', $_GET["ocurrency"], time() + 60*60*24*30, '/', $site_url);
	}
}
add_action('init','set_ocurrency_param', 1, 0 );


//DEFINE LA PASARELA DE PAGO POR DEFECTO PAFRA ESTE CASO PAYULATM
add_action( 'template_redirect', 'define_default_payment_gateway' );
function define_default_payment_gateway(){
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        // HERE define the default payment gateway ID
        $default_payment_id = 'subscription_payu_latam';
        WC()->session->set( 'chosen_payment_method', $default_payment_id );
    }
}

//OTRO PEDAZO DE LA LOGICA ESTA EN EL TEMA EN EL ARCHIVO alecta-child/woocommerce/myaccount/subscription-totals-table.php
//ESTE SIRVE PARA LA INFORMACION DE MY SUBSCRITION CUANDO EL USUARIO ESTA LOGEADO

///////////////////////////////
///////////////////////////////
//END SELECT CURRENCY LOGIC
///////////////////////////////
///////////////////////////////


function ochange_form_action(){
	
	$ocurrency = get_ocurrency();
	
	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
	  $lang = ICL_LANGUAGE_CODE;
	  if($lang == "es"){
		  $form_action = "/es/confirmar?ocurrency=$ocurrency";
	  }else if($lang == "en"){
	  	  $form_action = "/checkout?ocurrency=$ocurrency";
	  }
	}
	
	$html = "<script>";
	$html .= "jQuery( document ).ready(function() {";
	$html .= "jQuery('form').attr('action', '$form_action');";
	$html .= "});";
	$html .= "</script>";
	return $html;
}

add_shortcode('change_form_action', 'ochange_form_action');

//SET HTML SELECT
function set_html_for_currency_selector(){
	
	$options = get_option('select_box_ozone_multicurrency');
	write_log("options inside set_html_for_currency_selector");
	write_log($options);
	$select_options = explode(",", get_option('select_box_ozone_multicurrency')); 
	 ?>
  		<select style="margin-top: 20px; color: #262525" class="select-css" id="select_ocurrency">
     	   	<option value=""><?php _e('Select Currency', 'ozone-multicurrency'); ?></option>
			<?php 
				foreach ($select_options as $item) {	
					echo $item;
				}	
			?>
   	 	</select>
		<br />
	 <?php
}

//SET JS SELECT LOGIC
function set_js_for_currency_selector(){
    ?>
		  <script>
			
			
		  	ocurrency  = "<?php echo get_ocurrency();?>";
	
		  	jQuery( "#select_ocurrency" ).change(function() {
	  
		  	  url = origin + pathname + "?ocurrency="+jQuery(this).val();
		  	  window.location.href = url;
		  	});
	
		  	var pathname = window.location.pathname; // Returns path only (/path/example.html)
		  	var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
		  	var origin   = window.location.origin; 
			
		  	jQuery(document).ready( function (e) 
		  	{
		  		//ocurrency = getUrlParameter('ocurrency');
		  		
		  		jQuery("#select_ocurrency").val(ocurrency);
	
		  	});
			  
		  </script>
    <?php
}

//JS LOGIC FOR SELECT
add_action('wp_footer', 'set_js_for_currency_selector', 21, 0 ); 

//SET THE SHORTCODE
function my_ozone_multicurrency_select_box() { 
 
 	$translation = [];
	$translation["en"] = "United States Dollar";
	

	$html ="";
	$select_options = json_decode(get_option("ocurrency_select"));
	$html .= '<select style="color: #262525" class="select-css" id="select_ocurrency">';
	$html .= '<option value="">'.__('Select Currency', 'ozone-multicurrency').'</option>';
	foreach ($select_options as $x => $val) {	
		$html .= "<option value='$x'>$val</option>";
	}
	$html .='</select>';
	return $html;

} 
// register shortcode
add_shortcode('ozone_multicurrency_select_box', 'my_ozone_multicurrency_select_box');




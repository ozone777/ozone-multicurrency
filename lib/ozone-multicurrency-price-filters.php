<?php

//FUNCTION GET CURRENCY 
function get_ocurrency(){
		
	$wpml_current_language = ICL_LANGUAGE_CODE;
			
	    if(isset($_GET["ocurrency"])){
			//PRIORIDAD 1 PARMETRO GET
	       	$ocurrency = $_GET["ocurrency"];
			return $ocurrency;
	       
		}else if(is_user_logged_in()){
			//PRIORIDAD 2 MONEDA DE USUARIO
			$current_user = wp_get_current_user();
			$meta_ocurrency =  get_user_meta( $current_user->ID,  'ocurrency', true ); 
			if($meta_ocurrency != ""){
				$ocurrency = $meta_ocurrency;
				return $ocurrency;
			}else{
				$ocurrency = "USD";
				$ocurrency = strtoupper($ocurrency);
				return $ocurrency;
			}
		}else if(isset($_COOKIE['ocurrency'])){
			//PRIORIDAD 3 COOKIE, SIRVE PARA EL METODO DE WOOCOMMERCE ORDER REVIEW
			$ocurrency = $_COOKIE['ocurrency'];
			$ocurrency = strtoupper($ocurrency);
			return $ocurrency;
		}else{
			//PRIORIDAD 4 MONEDA DEFAULT USD
			$ocurrency = "COP";
			return $ocurrency;
		}
}

//FUNCTION GET OZONE MULTICURRENCY PRICE 
function get_oprice($product_id,$currency=null){
	
	if(!isset($currency))
		$currency = get_ocurrency();
	
	$price_meta_key = '_oprice_'.$currency;
	$price = get_post_meta($product_id, $price_meta_key, true );	
	return $price;
}


//FILTER USED IN PRODUCT SHOW
function oprice( $price, $product ){
	
	$my_default_lang = apply_filters('wpml_default_language', NULL );
	$product_origin_id = apply_filters( 'wpml_object_id', $product->get_id(), 'product', FALSE, $my_default_lang );
    $price = get_oprice($product_origin_id);
    return $price;
	
}
add_filter( 'woocommerce_product_get_price', 'oprice', 10, 2 );


//FILTER SIRVE PARA EL METODO DE WOOCOMMERCE ORDER REVIEW EN CHECKOUT SE CONSERVA LA SIMETRIA DE WOOCOMMERCE
function filter_woocommerce_cart_item_subtotal( $wc, $cart_item, $cart_item_key ) { 
    
	$ocurrency = get_ocurrency();
	$price = get_oprice($cart_item["product_id"], $ocurrency);
	$price = (float)($price * $cart_item["quantity"]);
	$price = apply_filters('ozone_multicurrency_price_format', $price,$ocurrency,$cart_item["product_id"]);
	return $price;
}; 

add_filter( 'woocommerce_cart_item_subtotal', 'filter_woocommerce_cart_item_subtotal', 99, 3 ); 

//FILTER USED FOR SUBSCRIPTIONS
function ozone_multicurrency_price_format_action($price,$ocurrency,$product_id){
	
	$price = number_format_i18n($price);
	$symbol = get_woocommerce_currency_symbol($ocurrency);
	$_product = wc_get_product($product_id);
	
	if(is_checkout()){
		if( class_exists( 'WC_Subscriptions_Product' ) && (WC_Subscriptions_Product::is_subscription( $_product )) ) {
			$period = WC_Subscriptions_Product::get_period($product_id);  
			if($period=="year")
				$cycle = " $ocurrency / ".__('year', 'ozone-multicurrency');
			else if($period=="month")
				$cycle = " $ocurrency / ".__('month', 'ozone-multicurrency');
			$price = "<p> $symbol $price $cycle</p>";
		}else{
			$price = "<p>$symbol $price $ocurrency</p>";
		}
	}else{
		$price = "<p> $symbol $price $ocurrency</p>";
	}
	
	return $price;
}

add_filter('ozone_multicurrency_price_format','ozone_multicurrency_price_format_action',10,3);
<?php

include_once( plugin_dir_path( __FILE__ ) . 'ozone-multicurrency-functions.php');

function ozone_multicurrency_admin_css() {
    wp_enqueue_style('ozone_multicurrency_admin', plugins_url('/ozone-multicurrency/css/admin.css'));
}
add_action('admin_enqueue_scripts', 'ozone_multicurrency_admin_css');

//PRODUCT EDITOR

// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields_omulti');
// Save Fields
///add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields_omulti()
{
    global $woocommerce, $post;
	
	
	$show_fields = true;
	if (class_exists('SitePress')) {
	    // WPML support. If WPML is active get product origin
		global $sitepress;
		$product_origin_id = icl_object_id($post->ID, 'post', true, $sitepress->get_default_language() );
		if($post->ID != $product_origin_id)
			$show_fields = false;
	}
	
	if($show_fields){
		
		$currency_symbols = ozone_get_woocommerce_currency_symbols();
		$select_options = json_decode(get_option("ocurrency_select"));
		$odefault_currency_value = get_option('odefault_currency');
		
    	echo '<div class="product_custom_field">';
		
		echo '<p><b>'.__('Ozone multicurrency prices', 'ozone-multicurrency').'</b></p>';
		
    	// Custom Product Text Field
		foreach ($select_options as $currency => $currency_label) {
			
				echo '<div class="ozone_multicurrency_blck">';
				echo "<p>$currency_label ($currency_symbols[$currency])</p>";
		    	woocommerce_wp_text_input(
		    	    array(
		    	        'id' => "_oprice_$currency",
		    	        'placeholder' => '',
		    	        'label' => "Product price ($currency_symbols[$currency])",
		    	        'desc_tip' => 'true'
		    	    )
		    	);
				
				echo '</div>';
				
				/*
				Aditional prices in woocommerce
				echo '<div class="ozone_multicurrency_blck">';
				
		    	woocommerce_wp_text_input(
		    	    array(
		    	        'id' => "_oprice_sale_$currency",
		    	        'placeholder' => '',
		    	        'label' => "Sale price ($currency_symbols[$currency])",
		    	        'desc_tip' => 'true'
		    	    )
		    	);
				
				echo '</div>';
				*/
			
		}
    	
    	echo '</div>';
	
	}
}


function save_woocommerce_product_custom_fields_omulti($post_id)
{
	
	$product = wc_get_product($post_id);
	$select_options = json_decode(get_option("ocurrency_select"));
	foreach ($select_options as $currency => $currency_label) {
		
		$oprice = '_oprice_'.$currency;
	 	if(isset($_POST[$oprice])){
  	      $product->update_meta_data($oprice, sanitize_text_field($_POST[$oprice]));
  	      $product->save();
	 	}
		
		$oprice = '_oprice_sale_'.$currency;
	 	if(isset($_POST[$oprice])){
  	      $product->update_meta_data($oprice, sanitize_text_field($_POST[$oprice]));
  	      $product->save();
	 	}
		
	}
	 

		  
}
add_action('woocommerce_process_product_meta', 'save_woocommerce_product_custom_fields_omulti');








/*add_filter( 'woocommerce_shipping_rate_cost', 'wp_kama_woocommerce_shipping_rate_cost_filter', 10, 2 );

function wp_kama_woocommerce_shipping_rate_cost_filter( $data_cost, $that ){

	// filter...
	write_log("data_cost:");
	write_log($data_cost);
	return 7777;
}
*/


function woocommerce_cart_product_price_oaction($product_price, $product){
	$ocurrency = get_ocurrency();
	$price = get_oprice($product->get_id(),$ocurrency);
	$price = apply_filters( 'ozone_multicurrency_price_format', $price,$ocurrency,$product->get_id());
	return $price;
}

add_filter( 'woocommerce_cart_product_price', 'woocommerce_cart_product_price_oaction',10,2);
//WOOCOMMERCE CHECKOUT


//return apply_filters( 'woocommerce_cart_subtotal', $cart_subtotal, $compound, $this );
function woocommerce_cart_subtotal_oaction($cart_subtotal, $compound, $object){
	$ocurrency = get_ocurrency();
	$cart_subtotal = 0;
	foreach ( WC()->cart->get_cart() as $cart_item ){
		$product_id = $cart_item['data']->get_id();
		$cart_subtotal+= get_oprice($product_id,$ocurrency) * $cart_item['quantity'];
	}
	$price = apply_filters( 'ozone_multicurrency_price_format', $cart_subtotal,$ocurrency,$product_id);
	return $price;
	
}
add_filter('woocommerce_cart_subtotal','woocommerce_cart_subtotal_oaction',10,3);

function woocommerce_cart_totals_order_total_html_oaction($value){
	
	$ocurrency = get_ocurrency();
	$cart_subtotal = 0;
	foreach ( WC()->cart->get_cart() as $cart_item ){
		$product_id = $cart_item['data']->get_id();
		$cart_subtotal+= get_oprice($product_id,$ocurrency) * $cart_item['quantity'];
	}
	$price = apply_filters( 'ozone_multicurrency_price_format', $cart_subtotal,$ocurrency,$product_id);
	
	//Add taxes here soon
	
	
	return $price;
}

add_filter("woocommerce_cart_totals_order_total_html","woocommerce_cart_totals_order_total_html_oaction",10,1);

// Outputting the hidden field in checkout page
function add_custom_checkout_hidden_field( $checkout ) {

    // Generating the VID number
	$currency = get_ocurrency();
    // Output the hidden field
    echo '<div id="user_link_hidden_checkout_field">
            <input type="hidden" class="input-hidden" name="order_currency" id="order_currency" value="' . $currency . '">
    </div>';
}

add_action( 'woocommerce_after_order_notes', 'add_custom_checkout_hidden_field' );


function select_tag_ocurrency(){

	$html ="";
	$select_options = json_decode(get_option("ocurrency_select"));
	$html .= '<select style="color: #262525; margin-top: 10px" class="select-css" id="select_ocurrency">';
	$html .= '<option value="">'.__('Select Currency', 'ozone-multicurrency').'</option>';
	foreach ($select_options as $x => $val) {	
		$html .= "<option value='$x'>$val</option>";
	}
	$html .='</select>';
	echo $html;
		
}

add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );

function change_default_checkout_country() {
	$ocurrency = get_ocurrency();
	write_log("change_default_checkout_country : $ocurrency");
	if($ocurrency == "USD")
  		return 'US'; 
	else if($ocurrency == "COP")
		return 'CO'; 
	else if($ocurrency == "CAD")
		return 'CA'; 
}



//OZONE MULTICURRRENCY OPTIONS

function load_ocurrency_selector(){

//CURRENCY SELECTOR IN CHECKOUT
if(get_option('oselect_box_in_checkout') == "true"){
	
	add_action( 'woocommerce_checkout_order_review', 'select_tag_ocurrency', 1, 0 );
	
}

//CURRENCY SELECTOR IN CART
if(get_option('oselect_box_in_cart') == "true"){
	
	add_action( 'woocommerce_before_cart', 'select_tag_ocurrency', 20, 0 );
	
}

//CURRENCY SELECTOR IN PRODUCT SHOW
if(get_option('oselect_box_in_product_show') == "true"){
	
	add_action( 'woocommerce_before_add_to_cart_form', 'select_tag_ocurrency',10,0 );

}

}

add_action('wp_loaded', 'load_ocurrency_selector');




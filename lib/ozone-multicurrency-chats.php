<?php

include_once( plugin_dir_path( __FILE__ ) . 'ozone-multicurrency-functions.php');

function ozone_multicurrency_whatsapp_chat_function($atts) { 
 
	
	if(isset($atts['icon_color']))
		$icon_color = $atts['icon_color'];
	else
		$icon_color = '#25d366';
	
	if(isset($atts['icon_color'])){
		$message = $atts['message'];
		$message = http_build_query(["text" =>$message]);
	}else{
		$message = "";
		$message = http_build_query(["text" =>$message]);
	}
	
	if(isset($atts['phone']))
		$phone = $atts['phone'];
	else
		$phone = "1111111111";
	
	$html ="";
	$html .="<a style='background-color:$icon_color;' href='https://api.whatsapp.com/send?phone=$phone&$message' class='float' target='_blank'>";
	$html .="<i class='fa fa-whatsapp my-float'></i>";
	$html .="</a>";
	return $html;

} 
// register shortcode
add_shortcode('ozone_multicurrency_whatsapp_chat', 'ozone_multicurrency_whatsapp_chat_function');


function ozone_multicurrency_add_chat_files() {
   // wp_enqueue_style('ozone_multicurrency_font_awesome', plugins_url('/ozone-multicurrency/css/font-awesome.css'));
}
add_action('wp_enqueue_scripts', 'ozone_multicurrency_add_chat_files');


function ozone_multicurrency_activate_chats(){
	
	$ozone_multicurrency_activate_chats = get_option("ozone_multicurrency_activate_chats");
	if($ozone_multicurrency_activate_chats == "true"){
	 	$ocurrency  = get_ocurrency();
	 	$ochat_content = get_option("ochat_$ocurrency");
		$ochat_type = get_option("ochat_type_$ocurrency");
	  	if($ochat_type == "shortcode")
	 		echo do_shortcode(wp_unslash($ochat_content));
		if($ochat_type == "javascript")
	 	   echo wp_unslash($ochat_content);
	}
 	
}

add_action("wp_footer","ozone_multicurrency_activate_chats");
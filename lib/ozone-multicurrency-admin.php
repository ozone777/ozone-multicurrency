<?php


include_once( plugin_dir_path( __FILE__ ) . 'ozone-multicurrency-functions.php');

/*///////////////////////////////
AJAX ACTIONS
https://developer.wordpress.org/reference/functions/check_ajax_referer/
*////////////////////////////////

//ADD CURRENCY
function add_currency(){
	$ajax_nonce = wp_create_nonce( "add_currency_nonce" );
	?>
 
	<script type="text/javascript">
	jQuery(document).ready(function($){
		 $("#add_currency").on("click", function(){
			 currency = $("#select_currency").val();
			 currency_label = $("#select_currency option:selected").text();
	    var data = {
	        action: 'add_currency',
	        security: '<?php echo $ajax_nonce; ?>',
	        currency: currency,
			currency_label: currency_label
	    };
	    $.post(ajaxurl, data, function(response) {
			$("#modal-content").dialog('close');
			$("#selected_currencies").append(`<p id='${data.currency}'><a href='#' currency='${data.currency}' class='remove_currency'><span style='color:red' class='dashicons dashicons-no-alt'></span></a> ${data.currency} ${data.currency_label} </p>`);
			$("#odefault_currency").append(new Option(data.currency_label,data.currency));
			
	    });
		return false;
		 });
	});
	</script>
	<?php
}

add_action( 'admin_footer', 'add_currency',0,11 ); // Write our JS below here

function add_currency_function() {
    check_ajax_referer( 'add_currency_nonce', 'security' );
	if(current_user_can('administrator') ){
		$currency_label = $_POST['currency_label'];
		$currency = $_POST['currency'];
		$ocurrency_select_array = json_decode(get_option("ocurrency_select"), true);
		if($ocurrency_select_array == ""){
			$ocurrency_select_array = array();
			$ocurrency_select_array["$currency"] = $currency_label;
		}else{
			write_log($ocurrency_select_array);
			$ocurrency_select_array["$currency"] = $currency_label;
		}
		$ocurrency_select_array_encoded = json_encode($ocurrency_select_array);
		update_option("ocurrency_select",$ocurrency_select_array_encoded);
    	echo sanitize_text_field( $_POST['currency_label'] );
    	die;
	}
}
add_action( 'wp_ajax_add_currency', 'add_currency_function' );

////END ADD CURRENCY//////////////////////////////////
//////////////////////////////////////////////////////

//REMOVE CURRENCY
function remove_currency(){
	$ajax_nonce = wp_create_nonce( "remove_currency_nonce" );
	?>
 
	<script type="text/javascript">
		
		
	jQuery(document).ready(function($){
		
		function remove_currency(){
		
		 	$(".remove_currency").on("click", function(){
			 	
			 	currency = $(this).attr("currency");
	   		 	var data = {
	        		action: 'remove_currency',
	        		security: '<?php echo $ajax_nonce; ?>',
	       		 	currency: currency,
	    		};
	    		$.post(ajaxurl, data, function(response) {
					console.log("Response");
					console.log(response);
					$("#"+response).remove();
	    	});
				return false;
		 	});
	 	}
		
		remove_currency();
	});
	</script>
	<?php
}

add_action( 'admin_footer', 'remove_currency',0,10 ); 

function remove_currency_function() {
    check_ajax_referer( 'remove_currency_nonce', 'security' );
	if(current_user_can('administrator') ){
		$currency = $_POST['currency'];
		$ocurrency_select_array = json_decode(get_option("ocurrency_select"), true);
		unset($ocurrency_select_array[$currency]);
		$ocurrency_select_array_encoded = json_encode($ocurrency_select_array);
		update_option("ocurrency_select",$ocurrency_select_array_encoded);
    	echo sanitize_text_field( $currency );
    	die;
	}
}
add_action( 'wp_ajax_remove_currency', 'remove_currency_function' );


// create custom plugin settings menurrr
add_action('admin_menu', 'ozone_multicurrency_plugin_create_menu');

function ozone_multicurrency_plugin_create_menu() {
	
	$logo_icon_path = plugins_url() . '/ozone-multicurrency/images/logo_ozone_icon.png';
	
	//create new top-level menu
	//add_menu_page('Ozone Dashboard', 'OMulticurrency', 'administrator', __FILE__, 'ozonewiwiw' , $logo_icon_path,6);
	
	add_menu_page('Ozone Multicurrency Plugin', 'OMulticurrency', 'manage_options', 'ozone-multicurrency', 'menu_admin_display',$logo_icon_path,6 );

	//call register settings function
	add_action( 'admin_init', 'register_ozone_multicurrency_plugin' );
}

function register_ozone_multicurrency_plugin() {
	//register our settings
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'select_box_ozone_multicurrency' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'oselect_box_in_checkout' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'oselect_box_in_product_show' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'oselect_box_in_cart' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'odefault_currency' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'display_guest_user_only' );
	register_setting( 'ozone_multicurrency_plugin-settings-group', 'valid_currencies');
	
	
	register_setting( 'ozone_multicurrency_plugin-settings-group','ozone_selected_currencies');
}

function page_tabs( $current = 'first' ) {
    $tabs = array(
        'first'   => __( 'Settings', 'plugin-textdomain' ), 
        'second'  => __( 'Chats', 'plugin-textdomain' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=ozone-multicurrency&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

function ozone_multicurrency_save_chats(){
	if((isset($_REQUEST['_wpnonce'])) && (current_user_can('administrator')) && (isset($_POST["ozone_multicurrency_save_chats"]))){
		
		$retrieved_nonce = $_REQUEST['_wpnonce'];
		if (!wp_verify_nonce($retrieved_nonce, 'ozone_multicurrency_save_chats' ) ) die( 'Failed security check' );
		
		if(isset($_POST['ozone_multicurrency_activate_chats']))
			update_option("ozone_multicurrency_activate_chats",$_POST['ozone_multicurrency_activate_chats']);
		else
			update_option("ozone_multicurrency_activate_chats","");
		
		foreach($_POST as $key => $value)
		{
			
			if(strpos($key, "ochat") !== false){
				write_log("key: $key");
				write_log("value: $value");
				update_option($key,$value);
			}
		}
	
	}
}

add_action("wp_loaded","ozone_multicurrency_save_chats");

function ozone_multicurrency_chats(){
	?>
	<h3><?php echo __( 'Add the shortcodes or chat scripts that correspond to each currency', 'ozone-multicurrency' )?></h3>
	<p><?php echo __( 'You can use the shortcode for the whatsapp chat that is built in Ozone Multicurrency plugin', 'ozone-multicurrency' )?></p>
	<p>Exmaple: [ozone_multicurrency_whatsapp_chat phone='1111111111' message='Hello pete!' icon_color='#b9cde0']</p>
	
	<form action="" method="post">
	    <input type="hidden" id="ozone_multicurrency_save_chats" name="ozone_multicurrency_save_chats" value="true">
		
	<?php wp_nonce_field('ozone_multicurrency_save_chats'); ?>
	
    <table class="form-table">
	
        <tr valign="top">
        <th scope="row"><?php echo __( 'Activate chats', 'ozone-multicurrency' )?> </th>
        <td>
			<input type="checkbox" id="ozone_multicurrency_activate_chats" name="ozone_multicurrency_activate_chats" value="true" <?php if(get_option('ozone_multicurrency_activate_chats') == "true") echo 'checked' ?>>
		</td>
        </tr>
		
		<?php
		
			$select_options = json_decode(get_option("ocurrency_select"));
			$html ="<div id='selected_currencies'>";
			foreach ($select_options as $x => $val) {
				$ochat_content = get_option("ochat_$x");
				$ochat_type = get_option("ochat_type_$x");
				
				echo "<tr valign='top'>";
				echo "<th scope='row'>$val</th>";
				echo "<td>";

				echo "<p><select name='ochat_type_$x' id='ochat_type_$x'>";
				echo "<option value=''>Select chat type</option>";
				
				if($ochat_type == "shortcode")
					echo "<option value='shortcode' selected>shortcode</option>";
				else
					echo "<option value='shortcode'>shortcode</option>";
				
				if($ochat_type == "javascript")
					echo "<option value='javascript' selected>javascript</option>";
				else
					echo "<option value='javascript'>javascript</option>";
				
				echo "</select></p>";
				
				echo "<textarea id='ochat_$x' name='ochat_$x' rows='8' cols='70'>";
				echo wp_unslash($ochat_content);
				echo "</textarea>";
				echo "</td>";
		        echo "</tr>";
			
			}
		
		?>
	</table>
	
	<?php submit_button("Save chats"); ?>
	</form>
	
	<?php
}

function ozone_multicurrency_setting_page(){
	?>
	

	<?php
	
	if ( !is_plugin_active('woocommerce/woocommerce.php')) {

	    $class = 'notice notice-error';
	    $message = "For Ozone Multicurrency to work correctly please install the following plugins: Woocommerce";
	    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 

	}
	?>

	<script>
	jQuery(function($) {
	    var $info = $("#modal-content");
	    $info.dialog({                   
	        'dialogClass'   : 'wp-dialog',           
	        'modal'         : true,
	        'autoOpen'      : false, 
	        'closeOnEscape' : true,  
			'width': 700,    
	        'buttons'       : {
	            "Close": function() {
	                $(this).dialog('close');
	            }
	        }
	    });
	    jQuery("#open-modal").click(function(event) {
	        event.preventDefault();
	        $info.dialog('open');
	    });
	});    
	</script>

	<?php
		$currencies = ozone_get_woocommerce_currencies();
	
		$html_select_currency="";
		$html_select_currency.='<select name="select_currency" id="select_currency">';
		$html_select_currency.="<option value=''>Select currency</option>";
		foreach($currencies as $x => $val) {
			$html_select_currency.="<option value='$x'>$val</option>";
		}
		$html_select_currency.="</select>";
	
	?>

	<div id="modal-content">
		<p><?php echo $html_select_currency;?> 
			<button type="button" id="add_currency" class="button-secondary"><i class="otgs-ico-add otgs-ico-sm"></i> <?php echo __( 'Add currency', 'ozone-multicurrency' )?></button>
		</p>
	</div>

	<div id="my-events-list"></div>


	<form method="post" action="options.php">
	    <?php settings_fields( 'ozone_multicurrency_plugin-settings-group' ); ?>
	    <?php do_settings_sections( 'ozone_multicurrency_plugin-settings-group' ); ?>
	    <table class="form-table">
		
	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Add currencies to select_tag', 'ozone-multicurrency' )?></th>
	        <td>
				<button type="button" id="open-modal" class="button-secondary"><i class="otgs-ico-add otgs-ico-sm"></i> <?php echo __( 'Add currency', 'ozone-multicurrency' )?></button>
			
			</td>
	        </tr>
		
		
	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Selected currencies', 'ozone-multicurrency' )?></th>
	        <td>
				<?php
				
				$select_options = json_decode(get_option("ocurrency_select"));
				$html ="<div id='selected_currencies'>";
				foreach ($select_options as $x => $val) {	
					$html .= "<p id='$x'><a href='#' currency='$x' class='remove_currency'><span style='color:red' class='dashicons dashicons-no-alt'></span></a> $x $val </p>";
				}
				$html.="</div>";
				echo $html;
				?>
			</td>
	        </tr>
		
      
		
	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Default currency', 'ozone-multicurrency' )?></th>
	        <td>
		   	
				<?php
				$odefault_currency_value = get_option('odefault_currency');
				$html ="";
				$select_options = json_decode(get_option("ocurrency_select"));
				$html .= '<select style="color: #262525" class="select-css" name="odefault_currency" id="odefault_currency">';
				$html .= '<option value="">'.__('Select Currency', 'ozone-multicurrency').'</option>';
				foreach ($select_options as $x => $val) {
					if($odefault_currency_value == $x)
						$html .= "<option value='$x' selected>$val</option>";
					else
						$html .= "<option value='$x'>$val</option>";
				}
				$html .='</select>';
				echo $html;
				?>
			
			
			</td>
	        </tr>
		

	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Display select_tag in woocommerce checkout', 'ozone-multicurrency' )?></th>
	        <td>
				<input type="checkbox" id="oselect_box_in_checkout" name="oselect_box_in_checkout" value="true" <?php if(get_option('oselect_box_in_checkout') == "true") echo 'checked' ?>>
			</td>
	        </tr>
		
		
	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Display select_tag in woocommerce cart', 'ozone-multicurrency' )?> </th>
	        <td>
				<input type="checkbox" id="oselect_box_in_cart" name="oselect_box_in_cart" value="true" <?php if(get_option('oselect_box_in_cart') == "true") echo 'checked' ?>>
			</td>
	        </tr>
        
	        <tr valign="top">
	        <th scope="row">
				<?php echo __( 'Display select_tag in woocommerce product show', 'ozone-multicurrency' )?> 
	        </th>
	        <td>
				<input type="checkbox" id="oselect_box_in_product_show" name="oselect_box_in_product_show" value="true" <?php if(get_option('oselect_box_in_product_show') == "true") echo 'checked' ?>>
			
			</td>
	        </tr>

	        <tr valign="top">
	        <th scope="row"><?php echo __( 'Multicurrency select_tag shortcode', 'ozone-multicurrency' )?></th>
	        <td>
				[ozone_multicurrency_select_box]
			</td>
	        </tr>
		
	    </table>
    
	    <?php submit_button(); ?>

	</form>



	<?php 
}

function menu_admin_display() {
	// Code displayed before the tabs (outside)
	// Tabs
	$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'first';
	page_tabs( $tab );

	if ( $tab == 'first' ) {
	    // add the code you want to be displayed in the first tab
		ozone_multicurrency_setting_page();
	}
	else {
	    // add the code you want to be displayed in the second tab
		ozone_multicurrency_chats();
	}
	// Code af
}
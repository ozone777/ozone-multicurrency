<?php
/**
 * Plugin Name: Ozone Multicurrency  
 * Plugin URI: http://ozonegroup.co
 * Description: This plugin add special behavior to Ozone Web Services.
 * Version: 1.0
 * Author: Peter Consuegra
 * Author URI: https://ozonegroup.co
 * License: All rigths reserved
 */


include_once( plugin_dir_path( __FILE__ ) . '/lib/ozone-multicurrency-functions.php');
include_once( plugin_dir_path( __FILE__ ) . '/lib/ozone-multicurrency-admin.php');
include_once( plugin_dir_path( __FILE__ ) . '/lib/ozone-multicurrency-woocommerce.php');
include_once( plugin_dir_path( __FILE__ ) . '/lib/ozone-multicurrency-chats.php');
include_once( plugin_dir_path( __FILE__ ) . '/lib/ozone-multicurrency-price-filters.php');

